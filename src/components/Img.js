import Component from './Component.js';
export default class Img extends Component {
	constructor(link) {
		super('img', { name: 'src', value: `${link}` }, null);
	}
}
