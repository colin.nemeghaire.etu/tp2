export default class Component {
	tagName;
	children;
	attribute;
	constructor(tagName, attribute, children) {
		this.tagName = tagName;
		this.children = children;
		this.attribute = attribute;
	}

	renderChildren() {
		if (this.children) {
			if (this.children instanceof Component) {
				return this.children.render();
			} else if (Array.isArray(this.children)) {
				let res = ``;
				this.children.forEach(element => {
					if (element instanceof Component) {
						res += element.render();
					} else {
						res += element;
					}
				});
				return res;
			} else {
				return this.children;
			}
		} else {
			return ``;
		}
	}

	render() {
		if (this.tagName) {
			if (this.attribute) {
				if (this.tagName == 'img') {
					return `<img ${this.attribute.name}="${this.attribute.value}"/>`;
				}
				return `<${this.tagName} ${this.attribute.name}="${
					this.attribute.value
				}">${this.renderChildren()}</${this.tagName}>`;
			}

			return `<${this.tagName}>${this.renderChildren()}</${this.tagName}>`;
		}
	}
}
