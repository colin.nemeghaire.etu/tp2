import Component from './components/Component.js';
import Img from './components/Img.js';
import data from './data.js';
import PizzaThumbnail from './components/PizzaThumbnail.js';
import PizzaList from './pages/PizzaList.js';
/*const title = new Component('h1', '', 'La carte');
document.querySelector('.pageTitle').innerHTML = title.render();*/

/*const img = new Component('img', {
	name: 'src',
	value:
		'https://images.unsplash.com/photo-1532246420286-127bcd803104?fit=crop&w=500&h=300',
});*/

const title = new Component('h1', null, ['La', ' ', 'carte']);
document.querySelector('.pageTitle').innerHTML = title.render();
//console.log(title.render());
/*const img = new Img(
	'https://images.unsplash.com/photo-1532246420286-127bcd803104?fit=crop&w=500&h=300'
);
document.querySelector('.pageContent').innerHTML = img.render();*/

/*const c = new Component('article', { name: 'class', value: 'pizzaThumbnail' }, [
	new Img(
		'https://images.unsplash.com/photo-1532246420286-127bcd803104?fit=crop&w=500&h=300'
	),
	'Regina',
]);
document.querySelector('.pageContent').innerHTML = c.render();*/
//console.log(c);

/*const pizza = data[0];
const pizzaThumbnail = new PizzaThumbnail(pizza);
document.querySelector('.pageContent').innerHTML = pizzaThumbnail.render();*/

// `data` est le tableau défini dans `src/data.js`
const pizzaList = new PizzaList(data);
document.querySelector('.pageContent').innerHTML = pizzaList.render();

Router.titleElement = document.querySelector('.pageTitle');
Router.contentElement = document.querySelector('.pageContent');
Router.routes = [{ path: '/', page: pizzaList, title: 'La carte' }];

Router.navigate('/'); // affiche 'La carte' dans .pageTitle
// et la pizzaList dans .pageContent
