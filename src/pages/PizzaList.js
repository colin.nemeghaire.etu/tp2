import Component from '../components/Component.js';
import PizzaThumbnail from '../components/PizzaThumbnail.js';

export default class PizzaList extends Component {
	data;
	constructor(data) {
		super();
		this.data = data;
	}

	render() {
		const thumb = this.data.map(
			pizza => new PizzaThumbnail(pizza).render() + ''
		);

		return new Component(
			'section',
			{ name: 'class', value: 'pizzaList' },
			thumb
		).render();
	}
}
